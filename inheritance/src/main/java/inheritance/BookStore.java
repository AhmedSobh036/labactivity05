package inheritance;

public class BookStore{

    public static void main(String[] args){
        Book[] books = new Book[5];
        books[0] = new Book("First Title", "First Author");
        books[1] = new ElectronicBook("Second Title", "Second Author", 10);
        books[2] = new Book("Third Title", "Third Author");
        books[3] = new ElectronicBook("Fourth Title", "Fourth Author", 20);
        books[4] = new ElectronicBook("Fifth Title", "Fifth Author", 30);

        for(int i = 0; i < books.length; i++){
            System.out.println(books[i]);
        }

    }
}