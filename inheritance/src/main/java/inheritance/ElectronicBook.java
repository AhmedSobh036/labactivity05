package inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int numberBytes){
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }

    public String toString(){
        String fromBase = super.toString();
        fromBase += " the number of Bytes is " + this.numberBytes;
        return fromBase;
    }

}