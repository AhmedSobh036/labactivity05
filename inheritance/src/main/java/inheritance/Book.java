package inheritance;
public class Book{
    protected String title;
    private String author;

    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    public String getTitle(){
        return this.title;
    }

    public String getAuthor(){
        return this.author;
    }

    public String toString(){
        return "The author of this book is " + this.author + " and the title is " + this.title;
    }
    
}